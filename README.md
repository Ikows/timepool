##### Wild Code School Strasbourg session Septembre 2018: TimePool
#Projet TimePool

TimePool est une application qui permet de gérer et partager vos disponibilités
en récupérant vos différents calendriers professionnels et personnels
depuis votre compte Google Calendar.
L'algorithme de TimePool fusion ainsi les calendriers que vous aurez selectionnés
et fait ressortir DIRE AUTREMENT vos créneaux horraires sur lesquels vous pourrez
prendre des rdv.

## Bien débuter, récuper une copie du projet en local

Pour permettre un travail en équipe efficace, le projet a été déployé sur Github.
Afin de récupérer le projet sur votre machine en local, cliquez sur le lien suivant:
https://github.com/WildCodeSchool/Strasbourg_0918_PHP_TimePool. 

Clonez le Repository:
```
git clone
```
Documenation officielle: https://help.github.com/articles/cloning-a-repository/

### Prérequis

Pour installer le projet localement, certains prérequis
et installations de librairies sont nécessaires:
* Utiliser php-7

* Base de données SQL

* Installer [Composer](https://getcomposer.org/) en suivant la documentation officielle:
https://getcomposer.org/doc/00-intro.md

* Afin d'initialiser Composer,
lancez la commande suivant à la racine du projet:
```
composer init
```

* Utiliser le Framework [Symfony](https://symfony.com/doc/current/index.html#gsc.tab=0):
suivez la documentation officielle:
 https://symfony.com/doc/current/setup.html


### Installation

Suivez les étapes suivantes afin de faire fonctionner votre environnement de développement:

* Lancer la récupération des librairies installées dans le Repository:
```
composer install
```
* Récupérez la structure de la Base de Donnée avec les commandes de la documentation Symfony:
https://symfony.com/doc/current/doctrine.html

* Installer le moteur de template [Twig](https://twig.symfony.com/doc/2.x/):
```
composer require "twig/twig:^2.0"
```

## Deploiement

Le déploiement n'étant pas réalisé par l'équipe de développeur initale,
il est laissé à la discrétion de la nouvelle équipe le choix de la méthode
de mise en production du projet.

## Coder avec

* [Symfony](https://symfony.com/doc/current/index.html#gsc.tab=0)
Pourquoi utiliser Symfony? https://symfony.com/six-good-reasons
* [Composer](https://getcomposer.org/) 

## Accéder à notre documentation

[Documentation utilisateur](https://drive.google.com/file/d/1iZlJdglETtusxJRZAr8NdOcofp5KamIJ/view?usp=sharing).

## Contributeur du projet

Si vous voulez nous contacter directement ou soumettre un bug
vous pouvez rejoindre notre communauté sur [Slack](hhttps://timepool.slack.com/messages/CE36JKX6X/details/).

## Auteurs

* **Manon Baillet** [Github](https://github.com/punckle) - [LinkedIn](https://www.linkedin.com/in/baillet-manon/)
* **Laura Lenik** [Github](https://github.com/LauraLNK) - [LinkedIn](https://www.linkedin.com/in/laura-lenik-434449147/)
* **Jessica Legrand** [Github](https://github.com/maintenantcjoey) - [LinkedIn](https://www.linkedin.com/in/jessica-legrand-b363a3171/)
* **Nicolas Coulaud** [Github](https://github.com/Ikows) - [LinkedIn](https://www.linkedin.com/in/nicolas-coulaud-3a25b7164/)
* **Nihad Zatric** [Github](https://github.com/Nihad96) - [LinkedIn](https://www.linkedin.com/in/nihadzatric/)
  
## License

Cf Copyrights

## Mentions spéciales

Merci à Thomas Parent, CTO de [Websurg](https://www.websurg.com/fr/), et à l'[IRCAD](https://www.ircad.fr/fr/) d'avoir placé leur confiance en notre équipe et d'avoir laissé libre cours
à nos idées.