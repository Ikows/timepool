<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddEventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choicesArray = [];
        for ($i = 0; $i < 24; $i++) {
            $k = $i;
            if ($k<10) {
                $k = '0' . $k;
            }
            $choicesArray += [$k => strval($k)];
        }
        $minArray = ['00' => '00', '15' => '15', '30' => '30', '45' => '45'];

        $builder
            ->add('startHour', ChoiceType::class, [
                'choices' => $choicesArray
            ])
            ->add('endHour', ChoiceType::class, [
                'choices' => $choicesArray
            ])
            ->add('startMin', ChoiceType::class, [
                'choices' => $minArray
            ])
            ->add('endMin', ChoiceType::class, [
                'choices' => $minArray
            ])
            ->add('startDay', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker'],
            ))
            ->add('endDay', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker'],
            ))
            ->add('summary', TextType::class, ['label' => 'Title'])
            ->add('location')
            ->add('description', TextareaType::class)
            ->add('create', SubmitType::class)
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
