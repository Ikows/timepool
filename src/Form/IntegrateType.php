<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IntegrateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('starting', ChoiceType::class, [
                'choices' => [
                    'This week' => 0,
                    'Next week' => 1
                ],
                'label' => 'Starting from'
            ])
            ->add('range', ChoiceType::class, [
                'choices' => [
                    '1 Week' =>1,
                    '2 Weeks' => 2,
                    '1 Month' => 5,
                    '2 Months' => 10
                ],
                'label' => 'Range'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
