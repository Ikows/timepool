<?php

namespace App\Form;

use App\Entity\User;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\File\File;

class SettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('familyName')
            ->add('givenName')
            ->add('biography', CKEditorType::class, array(
                'label' => "Introduce yourself in 2 or 3 sentences (if you want to)",
                'required' => false
            ))
            ->add('webLink', TextType::class, array(
                'label' => "Link to your website, social network profile, etc.",
                'required' => false,
                'attr' => [
                    'placeholder' => "Ex: http://www.yourwebsite.com"
                ]
            ))
            ->add('picture', FileType::class, array(
                'label' => "Modify your profile picture",
                'required' => false,
                'data_class' => null
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
