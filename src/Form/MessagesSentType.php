<?php

namespace App\Form;

use App\Entity\MessagesSent;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessagesSentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('createdAt')
            ->add('title')
            ->add('content')
            ->add('sender')
            ->add('receiver')
            ->add('messages', new MessagesType())
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MessagesSent::class,
        ]);
    }
}
