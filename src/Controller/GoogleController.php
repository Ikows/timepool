<?php

namespace App\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class GoogleController extends AbstractController
{
    /**
     * Link to this controller to start the "connect" process
     * @Route("/connect/google", name="connect_google")
     * @param ClientRegistry $clientRegistry
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('google')
            ->redirect(
                [
                \Google_Service_Calendar::CALENDAR,
                \Google_Service_Oauth2::USERINFO_EMAIL,
                \Google_Service_Oauth2::USERINFO_PROFILE],
                ['access_type' => 'offline']
            );
    }

    /**
     * Facebook redirects to back here afterwards
     *
     * @Route("/connect/google/check", name="connect_google_check")
     * @param ObjectManager $manager
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function connectCheckAction(ObjectManager $manager)
    {
        $user = $this->getUser();
        $display = $user->getDisplay();

        if (!$this->getUser()) {
            return new JsonResponse(array('status' => false, 'message' => "User not found!"));
        } else {
            if ($display == 0) {
                $user->setDisplay(1);
                $manager->persist($user);
                $manager->flush();
                return $this->redirectToRoute('select_calendar');
            } else {
                return $this->redirectToRoute('dashboard');
            }
        }
    }
}
