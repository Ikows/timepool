<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\AddingRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_USER")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/profile/{id}", name="profile")
     * @param User $user
     * @param UserRepository $repository
     * @param AddingRepository $addingRepository
     * @param UserRepository $userRepo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(User $user, AddingRepository $addingRepository, UserRepository $userRepo)
    {
        $relation = $userRepo->findAll();
        $actualUser = $this->getUser();
        $actualUser->getRelations()->initialize();

        return $this->render('profile/index.html.twig', [
            'user' => $user,
            'relation' => $relation,
            'repo' => $addingRepository,
            'userRepo' => $userRepo
        ]);
    }
}
