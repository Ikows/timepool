<?php

namespace App\Controller;

use App\Entity\ReceivedMessage;
use App\Entity\SentMessage;
use App\Entity\User;
use App\Form\MessagesType;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 *@IsGranted("ROLE_USER")
 */
class MessagesController extends AbstractController
{
    /**
     * Shows all received messages
     * @Route("/messages", name="messages")
     */
    public function index()
    {
        return $this->render('messages/index.html.twig');
    }

    /**
     * Shows the received message that was clicked on
     * @Route("/messages/show_inbox/{id}", name="messages_show")
     * @param ReceivedMessage $message
     * @param ObjectManager $manager
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\Response
     * @IsGranted("ROLE_USER")
     */
    public function showInbox(ReceivedMessage $message, ObjectManager $manager, SessionInterface $session)
    {
        //Ensure that an unauthorized user can not access the message
        if ($this->getUser()->getId() != $message->getUser()->getId()) {
            return $this->redirectToRoute('dashboard');
        }

        $message->setIsRead(1);
        $manager->persist($message);
        $manager->flush();

        $subject = $message->getTitle();
        $content = $message->getContent();
        //Information stored in the session to send it to the next view
        $session->set('title', $subject);
        $session->set('content', $content);

        return $this->render('messages/show_inbox.html.twig', [
            'message' => $message
        ]);
    }

    /**
     * Shows the sent message that was clicked on
     * @Route("/messages/show_sent/{id}", name="messages_show_sent")
     * @param SentMessage $message
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @IsGranted("ROLE_USER")
     */
    public function showSent(SentMessage $message, ObjectManager $manager)
    {
        //Ensure that an unauthorized user can not access the message
        if ($this->getUser()->getId() != $message->getUser()->getId()) {
            return $this->redirectToRoute('dashboard');
        }

        $message->setIsRead(1);
        $manager->persist($message);
        $manager->flush();

        return $this->render('messages/show_sent.html.twig', [
            'message' => $message
        ]);
    }

    /**
     * Send a message to a relation
     * @param User $relation
     * @param Request $request
     * @param ObjectManager $manager
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @Route("/messages/send/{id}", name="messages_send")
     * @IsGranted("ROLE_USER")
     */
    public function send(User $relation, Request $request, ObjectManager $manager, \Swift_Mailer $mailer)
    {
        $sent = new SentMessage();
        $user = $this->getUser();
        $form = $this->createForm(MessagesType::class, $sent);
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $sent->setCreatedAt(new \DateTime());
            //copying message to make received message
            $received = new ReceivedMessage($sent, $relation, $user);
            $sent->setUser($user)
                ->setReceiver($relation);

            $manager->persist($sent);
            $manager->persist($received);
            $manager->flush();

            //Send a mail to the relation
            $message = (new \Swift_Message("You have a new message on TimePool"))
                ->setFrom('timepool.tp@gmail.com')
                ->setTo($relation->getEmail())
                ->setBody(
                    $this->renderView(
                        'mail/mail_newMessage.html.twig',
                        ['relation' => $relation,
                            'user' => $user]
                    ),
                    'text/html'
                );
            $mailer->send($message);

            $this->addFlash('success', 'Your message has been delivered');
            return $this->redirectToRoute('messages');
        }
        return $this->render('messages/send.html.twig', [
            'form' => $form->createView(),
            'relation' => $relation
        ]);
    }

    /**
     * Shows all sent messages
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/messages/sent", name="messages_sent")
     * @IsGranted("ROLE_USER")
     */
    public function messagesSent()
    {
        $user = $this->getUser();

        return $this->render('messages/sent.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * Allows to delete the received messages
     * @param ReceivedMessage $message
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/messages/delete/{id}", name="messages_delete")
     * @IsGranted("ROLE_USER")
     */
    public function deleteInboxMessages(ReceivedMessage $message, ObjectManager $manager)
    {
        //Ensure that an unauthorized user can not access the message
        if ($this->getUser()->getId() != $message->getUser()->getId()) {
            return $this->redirectToRoute('dashboard');
        }

        $manager->remove($message);
        $manager->flush();

        $this->addFlash('warning', 'The message has been deleted');
        return $this->redirectToRoute('messages');
    }

    /**
     * Allows to delete the sent messages
     * @Route("/messages/delete_sent/{id}", name="messages_sent_delete")
     * @IsGranted("ROLE_USER")
     * @param SentMessage $message
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteSentMessages(SentMessage $message, ObjectManager $manager)
    {
        //Ensure that an unauthorized user can not access the message
        if ($this->getUser()->getId() != $message->getUser()->getId()) {
            return $this->redirectToRoute('dashboard');
        }

        $manager->remove($message);
        $manager->flush();

        $this->addFlash('warning', 'The message has been deleted');
        return $this->redirectToRoute('messages_sent');
    }

    /**
     * Allows to reply to a message
     * @Route("/messages/reply/{message}/{relation}", name="messages_reply")
     * @IsGranted("ROLE_USER")
     * @param User $relation
     * @param Request $request
     * @param ObjectManager $manager
     * @param SessionInterface $session
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function reply(
        ReceivedMessage $message,
        User $relation,
        Request $request,
        ObjectManager $manager,
        SessionInterface $session,
        \Swift_Mailer $mailer
    ) {
        //Ensure that an unauthorized user can not access the message
        if ($this->getUser()->getId() != $message->getUser()->getId()) {
            return $this->redirectToRoute('dashboard');
        }

        //We retrieve the informations stored in the session (cf: method showInbox in MessagesController)
        $title = 'Re: ' . $session->get('title');
        $content = '<br><br><i>' . 'You are replying to this message: ' . '"' . $session->get('content') . '"</i>';
        $user = $this->getUser();
        $sent = new SentMessage();
        $form = $this->createForm(MessagesType::class, $sent);
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $sent->setCreatedAt(new \DateTime());
            $received = new ReceivedMessage($sent, $relation, $user);
            $sent->setUser($user)
                ->setReceiver($relation);
            $manager->persist($sent);
            $manager->persist($received);
            $manager->flush();

            //Notification of a new message via mail
            $message = (new \Swift_Message("You have a new message on TimePool"))
                ->setFrom('timepool.tp@gmail.com')
                ->setTo($relation->getEmail())
                ->setBody(
                    $this->renderView(
                        'mail/mail_newMessage.html.twig',
                        ['relation' => $relation,
                            'user' => $user]
                    ),
                    'text/html'
                );
            $mailer->send($message);

            $this->addFlash('success', 'Your message has been delivered');
            return $this->redirectToRoute('messages');
        }

        return $this->render('messages/reply.html.twig', [
            'relation' => $relation,
            'title' => $title,
            'content' => $content,
            'form' => $form->createView()
        ]);
    }
}
