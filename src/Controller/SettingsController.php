<?php

namespace App\Controller;

use App\Form\SettingsType;
use App\Service\Clienting;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SettingsController extends AbstractController
{
    /**
     * Shows all the settings of TimePool
     * @Route("/settings", name="settings")
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param ObjectManager $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, ObjectManager $em)
    {
        $user = $this->getUser();
        $img = $user->getPicture();
        $settingsForm = $this->createForm(SettingsType::class, $user);
        $settingsForm->handleRequest($request);

        $client = Clienting::goClient($user, $em);

        $calendarApi = new \Google_Service_Calendar($client);
        $calendars = $calendarApi->calendarList->listCalendarList()->getItems();
        $calendarSel = [];

        foreach ($calendars as $calendar) {
            $calendarSel += [$calendar['summary'] => $calendar['id']];
        }

        $allCalendarsShow[] = array_pop($calendarSel);

        //Upload profile picture
        if ($settingsForm->isSubmitted() && $settingsForm->isValid()) {
            if ($request->files->get('settings')['picture'] != null) {
                $file = $user->getPicture();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('upload_directory'), $fileName);
                $user->setPicture($fileName);
            } else {
                $user->setPicture($img);
            }

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('settings');
        }

        //Allows to modify the calendars wanted on TimePool from Google Calendar
        $calendarsForm =$this->createFormBuilder($user)
            ->add('selectedCalendar', ChoiceType::class, [
                'choices' => $calendarSel,
                'expanded' => true,
                'multiple' => true,
            ])
            //Allows to change the calendar where the events created on TimePool will be saved
            ->add('addEventCalendar', ChoiceType::class, array(
                'label' => 'When you create an event on TimePool, add it to the calendar:',
                'choices' => $calendarSel
            ))
            ->getForm();
        $calendarsForm->handleRequest($request);

        if ($calendarsForm->isSubmitted() && $calendarsForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('dashboard');
        }

        return $this->render('settings/index.html.twig', [
            'settingsForm' => $settingsForm->createView(),
            'calendarsForm' => $calendarsForm->createView()
        ]);
    }
}
