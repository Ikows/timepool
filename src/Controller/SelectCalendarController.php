<?php

namespace App\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Google_Service_Calendar;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SelectCalendarController extends AbstractController
{
    /**
     * Retrieving the list of Google calendars of the connected user
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/select/calendar", name="select_calendar")
     * @IsGranted("ROLE_USER")
     */
    public function index(Request $request, ObjectManager $manager)
    {
        $user = $this->getUser();

        $client = new \Google_Client();
        $client->setAccessToken($user->getGoogleToken()->getToken());
        $service = new Google_Service_Calendar($client);
        //List all the calendars in Google Calendar
        $calendarList = $service->calendarList->listCalendarList();
        $calendarList = $calendarList->getItems();

        //Retrieve only the symmary and the id of each calendars
        $allCalendars = [];
        foreach ($calendarList as $calendars) {
            $allCalendars += [$calendars['summary'] => $calendars['id']];
        }

        //Don't show the last calendar of Google Calendar (WeekNumber)
        $calendarShow[] = array_pop($allCalendars);

        //Allows to chose the calendars we want to use on TimePool
        if (empty($allCalendars)) {
            return $this->redirectToRoute('no_calendar');
        } else {
            $form = $this->createFormBuilder($user)
                ->add('selectedCalendar', ChoiceType::class, array(
                    'choices' => $allCalendars,
                    'expanded' => true,
                    'multiple' => true,
                    'label' => 'Your calendars: '
                )) //Allows you to chose the calendar where you want to save the event created on TimePool
                ->add('addEventCalendar', ChoiceType::class, array(
                    'choices' => $allCalendars,
                    'label' => 'When you create an event on TimePool, add it to the calendar: '
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $user = $form->getData();
                $manager->persist($user);
                $manager->flush();

                return $this->redirectToRoute('dashboard');
            }

            return $this->render('select_calendar/index.html.twig', [
                'form' => $form->createView(),
                'calendarList' => $calendarList
            ]);
        }
    }

    /**
     * Render th view if you don't have a calendar on Google Calendar yet
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/nocalendar", name="no_calendar")
     * @IsGranted("ROLE_USER")
     */
    public function noCalendar()
    {
        return $this->render('select_calendar/noCalendar.html.twig');
    }
}
