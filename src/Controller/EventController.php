<?php

namespace App\Controller;

use App\Entity\PendingEvent;
use App\Service\EventCounter;
use App\Entity\ReceivedMessage;
use App\Repository\UserRepository;
use App\Repository\PendingEventRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Undocumented class
 * @IsGranted("ROLE_USER")
 */
class EventController extends AbstractController
{
    /**
     * @Route("/event", name="event")
     * @param PendingEventRepository $pendingEventRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PendingEventRepository $pendingEventRepository)
    {
        $user = $this->getUser();
        $createdEvents = $pendingEventRepository->findByEventCreator($user);

        $user->getAttending()->initialize();
        $attendingEvents = $user->getAttending();

        $data = EventCounter::countEvents($createdEvents, $attendingEvents);

        return $this->render('event/index.html.twig', $data);
    }

    /**
     * @Route("/event/attending", name="event_attending")
     */
    public function attendingEvents()
    {
        $user = $this->getUser();
        $user->getAttending()->initialize();
        $attendingEvents = $user->getAttending();

        return $this->render('event/attending.html.twig', [
            'events' => $attendingEvents
        ]);
    }

    /**
     * @Route("/event/created", name="event_created")
     * @param PendingEventRepository $pendingEventRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createdEvents(PendingEventRepository $pendingEventRepository)
    {
        $user = $this->getUser();
        $pendingEvents = $pendingEventRepository->findByEventCreator($user);
        return $this->render('event/created.html.twig', [
            'events' => $pendingEvents
        ]);
    }

    /**
     * @Route("/event/show/{id}", name="event_show")
     * @param PendingEvent $pendingEvent
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(PendingEvent $pendingEvent, UserRepository $userRepository)
    {
        //Ensure that an unauthorized user can not access the event
        $user = $this->getUser();
        if (!in_array($user->getId(), array_flip($pendingEvent->getParticipation()))
            and $user->getId() != $pendingEvent->getCreator()->getId()) {
            return $this->redirectToRoute('dashboard');
        }

        $creator = $pendingEvent->getCreator();
        $state = $pendingEvent->getState();

        return $this->render('event/show.html.twig', [
            'event' => $pendingEvent,
            'repo' => $userRepository,
            'user' => $user,
            'creator' => $creator,
            'state' => $state
        ]);
    }

    /**
     * @Route("/event/delete/{id}", name="event_delete")
     * @param ObjectManager $manager
     * @param PendingEvent $event
     * @param \Swift_Mailer $mailer
     * @param UserRepository $repository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function deleteEvent(
        ObjectManager $manager,
        PendingEvent $event,
        \Swift_Mailer $mailer,
        UserRepository $repository
    ) {
        $user = $this->getUser();

        //Ensure that an unauthorized user can not access the event
        if (!in_array($user->getId(), array_flip($event->getParticipation()))
            and $user->getId() != $event->getCreator()->getId()) {
            return $this->redirectToRoute('dashboard');
        }

        $relation = $event->getParticipation();
        $attenders = [];
        foreach ($relation as $key => $value) {
            $attenders[] = $repository->find($key);
        }

        $manager->remove($event);
        $manager->flush();

        //Creating message
        foreach ($attenders as $attender) {
            $message = new ReceivedMessage();
            $message
                ->setUser($attender)
                ->setCreatedAt(new \DateTime())
                ->setContent($this->renderView(
                    'messages/event_delete.html.twig',
                    ['user' => $user,
                        'event' => $event]
                ))
                ->setTitle($user->getFullname() . ' canceled an event');
            $manager->persist($message);
            $manager->flush();

            $mailMessage = (new \Swift_Message("You have a new message on TimePool"))
                ->setFrom('timepool.tp@gmail.com')
                ->setTo($attender->getEmail())
                ->setBody(
                    $this->renderView(
                        'mail/mail_newMessage.html.twig',
                        ['relation' => $attender,
                            'user' => $user]
                    ),
                    'text/html'
                );
            $mailer->send($mailMessage);
        }

        $this->addFlash('warning', 'The event has been cancelled');
        return $this->redirectToRoute('event', [
            'event' => $event
        ]);
    }

    /**
     * @Route("/event/accept/{id}", name="event_accept")
     * @param PendingEvent $event
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function acceptEvent(PendingEvent $event, ObjectManager $manager)
    {
        $user = $this->getUser();
        //Ensure that an unauthorized user can not access the event
        if (!in_array($user->getId(), array_flip($event->getParticipation()))
            and $user->getId() != $event->getCreator()->getId()) {
            return $this->json([
                'code' =>403,
                'status' => 'forbidden'
            ], 403);
        }

        $participation = $event->getParticipation();
        $participation[$this->getUser()->getId()] = 2;
        $event->setParticipation($participation);

        $launchable = true;
        foreach ($event->getParticipation() as $participant) {
            if ($participant == 1) {
                $launchable = false;
            }
        }

        if ($launchable) {
            $event->setState(2);
        }

        $manager->persist($event);
        $manager->flush();

        return $this->json([
            'code' => 200,
            'status' => 'accepted'
        ], 200);
    }


    /**
     * @Route("/event/decline/{id}", name="event_decline")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function declineEvent(PendingEvent $event, ObjectManager $manager)
    {
        $user = $this->getUser();
        //Ensure that an unauthorized user can not access the event
        if (!in_array($user->getId(), array_flip($event->getParticipation()))
            and $user->getId() != $event->getCreator()->getId()) {
            return $this->json([
                'code' =>403,
                'status' => 'forbidden'
            ], 403);
        }

        $participation = $event->getParticipation();
        $participation[$this->getUser()->getId()] = 0;
        $event->setParticipation($participation);

        $launchable = true;
        foreach ($event->getParticipation() as $participant) {
            if ($participant != 1) {
                $launchable = false;
            }
        }
        if ($launchable) {
            $event->setState(2);
        }

        $manager->persist($event);
        $manager->flush();

        return $this->json([
            'code' =>200,
            'status' => 'declined'
        ], 200);
    }
}
