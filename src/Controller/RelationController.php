<?php

namespace App\Controller;

use App\Entity\Adding;
use App\Entity\ReceivedMessage;
use App\Entity\User;
use App\Repository\AddingRepository;
use App\Repository\UserRepository;
use App\Service\KeyGenerator;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_USER")
 */
class RelationController extends AbstractController
{
    /**
     * Shows all the user of TimePool and allows to connect with them
     * @param UserRepository $repository
     * @param AddingRepository $addingRepository
     * @param UserRepository $userRepo
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/relation", name="relation")
     * @IsGranted("ROLE_USER")
     */
    public function index(
        UserRepository $repository,
        AddingRepository $addingRepository,
        UserRepository $userRepo
    ) {
        $relation = $repository->findAll();
        $user = $this->getUser();
        $user->getRelations()->initialize();

        return $this->render('relation/index.html.twig', [
            'relation' => $relation,
            'repo' => $addingRepository,
            'userRepo' => $userRepo
        ]);
    }

    /**
     * @Route("/relation/message/{id}", name="relation_message")
     * @param User $relation
     * @param Request $request
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addMessageRelation(User $relation, Request $request, SessionInterface $session)
    {
        $form = $this->createFormBuilder()
            ->add('content', TextareaType::class, array(
                'label' => 'If you want, you can add a message to your request:'
            ))
            ->getForm();

        $form->handleRequest(($request));

        if ($form->isSubmitted() && $form->isValid()) {
            $messageContent = $form->getData();
            $session->set('content', $messageContent['content']);

            return $this->redirectToRoute('relation_add', [
                'id' => $relation->getId()
            ]);
        }

        return $this->render('relation/messages.html.twig', [
            'user' => $relation,
            'form' => $form->createView()
        ]);
    }

    /**
     * @param User $relation
     * @param ObjectManager $manager
     * @param \Swift_Mailer $mailer
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     * @Route("/relation/add/{id}", name="relation_add")
     * @IsGranted("ROLE_USER")
     */
    public function add(
        User $relation,
        ObjectManager $manager,
        \Swift_Mailer $mailer,
        SessionInterface $session
    ) {
        $adding = new Adding();
        $user = $this->getUser();
        $adding->setAsker($user->getId())
            ->setAsked($relation->getId())
            ->setCle(KeyGenerator::generate());
        $manager->persist($adding);
        $manager->flush();

        $messageContent = $session->get('content');

        //Creating message
        $message = new ReceivedMessage();
        $message
            ->setUser($relation)
            ->setCreatedAt(new \DateTime())
            ->setContent($this->renderView(
                'messages/relation.html.twig',
                ['user' => $user,
                'adding' => $adding,
                'content' => $messageContent]
            ))
            ->setTitle($user->getFullname(). ' wants to add you as a relation');
        $manager->persist($message);
        $manager->flush();

        $mailMessage = (new \Swift_Message("You have a new message on TimePool"))
            ->setFrom('timepool.tp@gmail.com')
            ->setTo($relation->getEmail())
            ->setBody(
                $this->renderView(
                    'mail/mail_newMessage.html.twig',
                    ['relation' => $relation,
                        'user' => $user]
                ),
                'text/html'
            );
        $mailer->send($mailMessage);

        $this->addFlash('success', 'The invitaion for connection has been sent');
        return $this->redirectToRoute('relation');
    }

    /**
     * Shows the relations of the connected user
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/relation/show", name="relation_show")
     * @IsGranted("ROLE_USER")
     */
    public function show()
    {
        $user = $this->getUser();
        $user->getRelations()->initialize();

        $relations = $user->getRelations();
        return $this->render('relation/show_relations.html.twig', [
            'relations' => $relations
        ]);
    }

    /**
     * Allows to delete a relation
     * @Route("/relation/delete/{id}", name="relation_delete")
     * @param User $relation
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(User $relation, ObjectManager $manager)
    {
        $user = $this->getUser();
        $user->removeRelation($relation);
        $relation->removeRelation($user);
        $manager->persist($user);
        $manager->persist($relation);
        $manager->flush();

        return $this->redirectToRoute('relation_show');
    }

    /**
     * @Route("/relation/accept/{cle}", name="relation_accept")
     * @param Adding $adding
     * @param ObjectManager $manager
     * @param UserRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function accept(Adding $adding, ObjectManager $manager, UserRepository $repository)
    {
        $user = $repository->find($adding->getAsker());
        $relation = $repository->find($adding->getAsked());
        $user->addRelation($relation);
        $relation->addRelation($user);
        $manager->persist($user);
        $manager->persist($relation);
        $manager->remove($adding);
        $manager->flush();

        return $this->render('relation/relation_accept.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * Allows to delete an invitation that isn't accepted yet
     * @param User $relation
     * @param AddingRepository $addingRepository
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/delete/invitation/{id}", name="delete_invitation")
     * @IsGranted("ROLE_USER")
     */
    public function deleteInvitation(User $relation, AddingRepository $addingRepository, ObjectManager $manager)
    {
        $user = $this->getUser();
        if ($addingRepository->findOneByUserRelation($user, $relation)) {
            $adding = $addingRepository->findOneByUserRelation($user, $relation);
            $manager->remove($adding);
            $manager->flush();
            $this->addFlash(
                'success',
                'Your invitation has been successfully deleted'
            );
        } else {
            $adding = $addingRepository->findOneByUserRelation($relation, $user);
            $manager->remove($adding);
            $manager->flush();
            $this->addFlash(
                'success',
                'Your invitation has been successfully deleted'
            );
        }

        return $this->redirectToRoute('relation');
    }
}
