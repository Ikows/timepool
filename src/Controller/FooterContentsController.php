<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class FooterContentsController extends AbstractController
{
    /**
     * @Route("/footer/index", name="footer_index")
     */
    public function index()
    {
        return $this->render('footer_contents/index.html.twig');
    }

    /**
     * @Route("/footer/copyright", name="footer_copyright")
     */
    public function copyright()
    {
        return $this->render('footer_contents/copyright.html.twig');
    }

    /**
     * @Route("/footer/about", name="footer_about")
     */
    public function about()
    {
        return $this->render('footer_contents/about.html.twig');
    }

    /**
     * Contact form to ask for further information
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     * @Route("/footer/contact", name="footer_contact")
     */
    public function contact(Request $request, \Swift_Mailer $mailer): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $from = $form['email']->getData();
            $subject = $form['subject']->getData();
            $emailMessage = $form['message']->getData();
            $message = (new \Swift_Message($subject))
                ->setFrom($from)
                ->setTo('timepool.tp@gmail.com')
                ->setBody(
                    $this->renderView(
                        'mail/mail_contact.html.twig',
                        ['from' => $from,
                        'subject' => $subject,
                        'message' => $emailMessage]
                    ),
                    'text/html'
                );

            $mailer->send($message);

            $this->addFlash('success', 'Votre email a bien été envoyé');
            $this->redirectToRoute('footer_contact');
        }

        return $this->render('footer_contents/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
