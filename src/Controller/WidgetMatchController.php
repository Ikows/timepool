<?php
namespace App\Controller;

use App\Entity\PendingEvent;
use App\Entity\ReceivedMessage;
use App\Form\AddEventType;
use App\Repository\UserRepository;
use App\Service\EventCreator;
use App\Service\Merge;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class WidgetMatchController extends AbstractController
{
    /**
     * @Route("/widget/match", name="widget_match")
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, SessionInterface $session)
    {
        $user = $this->getUser();

        $user->getRelations()->initialize();

        $relations = [];
        foreach ($user->getRelations() as $relation) {
            $relations += [$relation->getFullname() => $relation->getId()];
        }

        $form = $this->createFormBuilder()
            ->add('submit', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            if ($data = $request->request->get('rel')) {
                $relationsForMatch = [];
                foreach ($data as $rel) {
                    $relationsForMatch[] += $rel;
                }

                $session->set('relations', $relationsForMatch);

                return $this->redirectToRoute('widget_match_result');
            } else {
                $this->addFlash('warning', 'You must select at least one relation to find a match !');
            }
        }

        return $this->render('widget_match/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/match/result", name="widget_match_result")
     * @param UserRepository $repository
     * @param ObjectManager $em
     * @param Merge $merge
     * @param SessionInterface $session
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @IsGranted("ROLE_USER")
     */
    public function matchResult(
        UserRepository $repository,
        ObjectManager $em,
        Merge $merge,
        SessionInterface $session,
        Request $request
    ) {

        // Creating form
        $form = $this->createForm(AddEventType::class);
        $form->handleRequest($request);

        // Retrieving user and attenders
        $user = $this->getUser();
        $relationsForMatch = $session->get('relations');
        $attenders = $repository->findById($relationsForMatch);

        // On submit
        if ($form->isSubmitted() and $form->isValid()) {
            // Creating participation variable with attenders ID as key and 1 as default value,
            // 0 means declined, 1 not decided, 2 accepted
            $participation = [];
            foreach ($relationsForMatch as $id) {
                $participation += [$id => 1];
            }

            // Creating and filing Pending event object
            $pendingEvent = new PendingEvent();
            $data = $request->request->get('add_event');
            $pendingEvent
                ->setStartTime(new
                \DateTime($data['startDay'] . ' ' . $data['startHour'] . ':' . $data['startMin'] . ':00'))
                ->setEndTime(new
                \DateTime($data['endDay'] . ' ' . $data['endHour'] . ':' . $data['endMin'] . ':00'))
                ->setSummary($data['summary'])
                ->setLocation($data['location'])
                ->setDescription($data['description'])
                ->setCreator($user)
                ->setParticipation($participation);
            foreach ($attenders as $attender) {
                $pendingEvent->addAttender($attender);
            }
            $em->persist($pendingEvent);
            $em->flush();

            //Creating message for the creator of the event
            $message = new ReceivedMessage();
            $message
                ->setUser($user)
                ->setCreatedAt(new \DateTime())
                ->setTitle('You\'ve created this event :  '. $pendingEvent->getSummary())
                ->setContent($this->renderView('messages/multiMessage.html.twig', [
                    'event' => $pendingEvent,
                    'profile' => 'creator'
                ]));
            $em->persist($message);
            $em->flush();

            // Creating message for all attenders
            foreach ($attenders as $attender) {
                $message = new ReceivedMessage();
                $message
                    ->setUser($attender)
                    ->setCreatedAt(new \DateTime())
                    ->setTitle($user->getFullname() . ' wants to create an event with you !')
                    ->setContent($this->renderView('messages/multiMessage.html.twig', [
                        'event' => $pendingEvent,
                        'attender' => $attender->getFullname(),
                        'profile' => 'attender'
                    ]));
                $em->persist($message);
            }
            $em->flush();

            $this->addFlash('success', 'Invitations has been sent !');

            return $this->redirectToRoute('widget_match_result');
        }


        $users = [$user];
        foreach ($relationsForMatch as $id) {
            $users[] = $repository->find($id);
        }

        $event = $merge->mergeMultiple($users, $em);

        $week = new \App\Service\Week($_GET['day'] ?? null, $_GET['month'] ?? null, $_GET['year'] ?? null) ;
        return $this->render('widget_match/result.html.twig', [
            'week' => $week,
            'event' => $event,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/match/event/launch/{event}", name="match_event_launch")
     * @param PendingEvent $pendingEvent
     * @param ObjectManager $manager
     * @param UserRepository $userRepository
     * @IsGranted("ROLE_USER")
     */
    public function eventLaunch(
        PendingEvent $event,
        ObjectManager $manager,
        UserRepository $userRepository,
        EventCreator $eventCreator
    ) {
        //Retrieving participants of the event and building an array
        // that contains only those who accept or did not answer + the event creator
        $participants = $event->getParticipation();
        $actualParticipants = [$this->getUser()];
        foreach ($participants as $participant => $state) {
            if ($state == 1 or $state == 2) {
                $actualParticipants[] = $userRepository->find($participant);
            }
        }
        $eventCreator->createMultiple($actualParticipants, $manager, $event);

        return $this->redirectToRoute('weekdget_calendar');
    }
}
