<?php

namespace App\Controller;

use App\Entity\SharedCalendar;
use App\Form\IntegrateType;
use App\Service\Clienting;
use App\Service\Merge;
use App\Service\Week;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class WidgetIntegrateController extends AbstractController
{
    /**
     * @Route("/widget/integrate", name="widget_integrate")
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param ObjectManager $manager
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index(Request $request, ObjectManager $manager, SessionInterface $session)
    {
        $form = $this->createForm(IntegrateType::class);
        $form->handleRequest($request);
        $display = false;

        if ($form->isSubmitted() and $form->isValid()) {
            $data = $form->getData();
            $user = $this->getUser();
            $client =Clienting::goClient($user, $manager);
            $event = Merge::merge($client, $user, null, ['starting' => $data['starting'], 'timeMax' => $data['range']]);
            $session->set('event', $event);
            $session->set('from', $data['starting']);
            $session->set('range', $data['range']);
            $display = true;
        }

        return $this->render('widget_integrate/index.html.twig', [
            'form' =>  $form->createView(),
            'display' => $display,
        ]);
    }

    /**
     * @Route("/widget/integrate/preview", name="integrate_preview")
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function preview(SessionInterface $session)
    {
        $week = new \App\Service\Week($_GET['day'] ?? null, $_GET['month'] ?? null, $_GET['year'] ?? null) ;

        if ($session->get('from') == 1) {
            $week = $week->nextWeek();
        }

        return $this->render('widget_integrate/frame_preview.html.twig', [
            'event' => $session->get('event'),
            'week' => $week
        ]);
    }

    /**
     * @Route("/widget/integrate/view/{sand}", name="integrate_view")
     * @param SharedCalendar $sharedCalendar
     * @param Week $week
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function frameView(SharedCalendar $sharedCalendar)
    {
        $startDateTime = $sharedCalendar->getStartFrom();
        $week = new \App\Service\Week(
            intval($startDateTime->format('d')),
            intval($startDateTime->format('m')),
            intval($startDateTime->format('Y'))
        );

        return $this->render('widget_integrate/frame_view.html.twig', [
            'event' => $sharedCalendar->getEvent(),
            'week' => $week
        ]);
    }

    /**
     * @Route("/widget/integrate/generate", name="integrate_generate")
     * @param SessionInterface $session
     * @param ObjectManager $manager
     * @IsGranted("ROLE_USER")
     */
    public function iframeGenerate(SessionInterface $session, ObjectManager $manager)
    {
        $sharedCalendar = new SharedCalendar($session->get('event'));
        $week = new Week();

        if ($session->get('from') == 1) {
            $week = $week->nextWeek();
        }
        $week = $week->showDate();

        $sharedCalendar->setStartFrom($week);
        $sharedCalendar->setWeekRange($session->get('range'));
        $manager->persist($sharedCalendar);
        $manager->flush();

        return $this->json([
            'sand' => $sharedCalendar->getSand()
        ], 200);
    }
}
