<?php

namespace App\Controller;

use App\Form\AddEventType;
use App\Service\Clienting;
use App\Service\EventCreator;
use App\Service\Merge;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WidgetCalendarController extends AbstractController
{
    /**
     * Shows the TimePool calendar
     * @Route("/widget/calendar", name="weekdget_calendar")
     * @IsGranted("ROLE_USER")
     * @param ObjectManager $manager
     * @param Request $request
     * @param EventCreator $eventCreator
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index(ObjectManager $manager, Request $request, EventCreator $eventCreator)
    {
        $user = $this->getUser();
        $week = new \App\Service\Week($_GET['day'] ?? null, $_GET['month'] ?? null, $_GET['year'] ?? null) ;

        //Fetching Google Client with user access
        $client = Clienting::goClient($user, $manager);

        //Fetching array containing the unavailable time slots
        $event = Merge::merge($client, $user, $week, ['timeMax' => 1]);

        $form = $this->createForm(AddEventType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $data = $request->request->get('add_event');

            $eventCreator->create($user, $client, $data);

            $this->addFlash('success', 'Your event has been successfully created');

            return $this->redirectToRoute('weekdget_calendar');
        }
        dump($event);
        return $this->render('widget_calendar/index.html.twig', [
            'week' => $week,
            'event' => $event,
            'form' => $form->createView()
        ]);
    }
}
