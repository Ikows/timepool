<?php

namespace App\Controller;

use App\Repository\AddingRepository;
use App\Service\Clienting;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LoginController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     */
    public function login()
    {
        return $this->render('security/login.html.twig', [
            'login' => true
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/login", name="login_role")
     */
    public function roleLogin()
    {
        return $this->redirectToRoute('app_login');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
    }

    /**
     * @param Request $request
     * @param ObjectManager $objectManager
     * @param TokenStorageInterface $storage
     * @param SessionInterface $session
     * @param AddingRepository $repository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/delete/account", name="deleteAccount")
     * @IsGranted("ROLE_USER")
     */
    public function deleteAccount(
        Request $request,
        ObjectManager $objectManager,
        TokenStorageInterface $storage,
        SessionInterface $session,
        AddingRepository $repository
    ) {
        $user = $this->getUser();
        $client = Clienting::goClient($user, $objectManager);
        $form = $this->createFormBuilder()
            ->add('Yes', SubmitType::class, [
                'label' => 'Yes, delete my account',
                'attr' => [
                    'class' => 'btn btn-danger w-100'
                ]
            ])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $client->revokeToken();
            $addings= $repository->findByActive($user->getId());
            foreach ($addings as $adding) {
                $objectManager->remove($adding);
            }
            $storage->setToken(null);
            $session->invalidate();
            $objectManager->remove($user);
            $objectManager->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/delete_account.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
