<?php
namespace App\Controller;

use App\Entity\SharedCalendar;
use App\Form\ShareType;
use App\Repository\SharedCalendarRepository;
use App\Service\Clienting;
use App\Service\Merge;
use App\Service\Week;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WidgetShareController extends AbstractController
{
    /**
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     * @Route("/widget/share", name="widget_share")
     * @IsGranted("ROLE_USER")
     */
    public function index(Request $request, \Swift_Mailer $mailer, ObjectManager $manager): Response
    {
        $user = $this->getUser();
        //Fetching Google Client with user access
        $client = Clienting::goClient($user, $manager);

        $week = new \App\Service\Week();
        $date = $week->showDate()->format(DATE_RFC3339);
        //Fetching array containing the unavailable time slots
        $event = Merge::merge($client, $user);

        $sharedCalendar = new SharedCalendar($event);
        $sharedCalendar->setStartFrom(new \DateTime());
        $manager->persist($sharedCalendar);
        $manager->flush();

        $form = $this->createForm(ShareType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $from = $user->getEmail();
            $emailFrom = $user->getGivenName() . ' ' . $user->getFamilyName();
            $to = $form['email']->getData();
            $emailMessage = $form['message']->getData();
            $message = (new \Swift_Message($emailFrom . ' via TimePool'))
                ->setFrom($from)
                ->setTo($to)
                ->setBody(
                    $this->renderView(
                        'mail/mail_share.html.twig',
                        ['from' => $emailFrom,
                            'message' =>$emailMessage,
                            'sand' => $sharedCalendar->getSand()
                        ]
                    ),
                    'text/html'
                );

            $mailer->send($message);

            $this->addFlash(
                'success',
                "Your email has been sent"
            );
        }

        return $this->render('widget_share/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/widget/share/response/{sand}", name="share_response")
     * @param SharedCalendar $sharedCalendar
     * @return Response
     * @throws \Exception
     */
    public function share(SharedCalendar $sharedCalendar)
    {
        $week = new \App\Service\Week($_GET['day'] ?? null, $_GET['month'] ?? null, $_GET['year'] ?? null) ;
        return $this->render('widget_share/share.html.twig', [
            'week' => $week,
            'event' => $sharedCalendar->getEvent()
        ]);
    }
}
