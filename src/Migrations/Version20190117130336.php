<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190117130336 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE adding (id INT AUTO_INCREMENT NOT NULL, cle VARCHAR(255) NOT NULL, asker INT NOT NULL, asked INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) DEFAULT NULL, family_name VARCHAR(255) NOT NULL, given_name VARCHAR(255) NOT NULL, selected_calendar LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', unselected_calendar LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', picture VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, google_token LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', fullname VARCHAR(255) NOT NULL, display TINYINT(1) NOT NULL, add_event_calendar VARCHAR(255) NOT NULL, biography LONGTEXT DEFAULT NULL, web_link VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_user (user_source INT NOT NULL, user_target INT NOT NULL, INDEX IDX_F7129A803AD8644E (user_source), INDEX IDX_F7129A80233D34C1 (user_target), PRIMARY KEY(user_source, user_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_pending_event (user_id INT NOT NULL, pending_event_id INT NOT NULL, INDEX IDX_4AB525FFA76ED395 (user_id), INDEX IDX_4AB525FFF63424AE (pending_event_id), PRIMARY KEY(user_id, pending_event_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE received_message (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, sender_id INT DEFAULT NULL, created_at DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, is_read TINYINT(1) NOT NULL, INDEX IDX_145E3A26A76ED395 (user_id), INDEX IDX_145E3A26F624B39D (sender_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pending_event (id INT AUTO_INCREMENT NOT NULL, summary VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, start_time DATETIME NOT NULL, end_time DATETIME NOT NULL, creator LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', participation LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', state INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shared_calendar (id INT AUTO_INCREMENT NOT NULL, event LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', sand VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sent_message (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, receiver_id INT NOT NULL, created_at DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, is_read TINYINT(1) NOT NULL, INDEX IDX_CC340760A76ED395 (user_id), INDEX IDX_CC340760CD53EDB6 (receiver_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_user ADD CONSTRAINT FK_F7129A803AD8644E FOREIGN KEY (user_source) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_user ADD CONSTRAINT FK_F7129A80233D34C1 FOREIGN KEY (user_target) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_pending_event ADD CONSTRAINT FK_4AB525FFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_pending_event ADD CONSTRAINT FK_4AB525FFF63424AE FOREIGN KEY (pending_event_id) REFERENCES pending_event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE received_message ADD CONSTRAINT FK_145E3A26A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE received_message ADD CONSTRAINT FK_145E3A26F624B39D FOREIGN KEY (sender_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sent_message ADD CONSTRAINT FK_CC340760A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sent_message ADD CONSTRAINT FK_CC340760CD53EDB6 FOREIGN KEY (receiver_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_user DROP FOREIGN KEY FK_F7129A803AD8644E');
        $this->addSql('ALTER TABLE user_user DROP FOREIGN KEY FK_F7129A80233D34C1');
        $this->addSql('ALTER TABLE user_pending_event DROP FOREIGN KEY FK_4AB525FFA76ED395');
        $this->addSql('ALTER TABLE received_message DROP FOREIGN KEY FK_145E3A26A76ED395');
        $this->addSql('ALTER TABLE received_message DROP FOREIGN KEY FK_145E3A26F624B39D');
        $this->addSql('ALTER TABLE sent_message DROP FOREIGN KEY FK_CC340760A76ED395');
        $this->addSql('ALTER TABLE sent_message DROP FOREIGN KEY FK_CC340760CD53EDB6');
        $this->addSql('ALTER TABLE user_pending_event DROP FOREIGN KEY FK_4AB525FFF63424AE');
        $this->addSql('DROP TABLE adding');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_user');
        $this->addSql('DROP TABLE user_pending_event');
        $this->addSql('DROP TABLE received_message');
        $this->addSql('DROP TABLE pending_event');
        $this->addSql('DROP TABLE shared_calendar');
        $this->addSql('DROP TABLE sent_message');
    }
}
