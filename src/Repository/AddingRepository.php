<?php

namespace App\Repository;

use App\Entity\Adding;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Adding|null find($id, $lockMode = null, $lockVersion = null)
 * @method Adding|null findOneBy(array $criteria, array $orderBy = null)
 * @method Adding[]    findAll()
 * @method Adding[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Adding::class);
    }

    // /**
    //  * @return Adding[] Returns an array of Adding objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findOneByUserRelation($user, $relation): ?Adding
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.asker = :user')
            ->andWhere('a.asked = :relation')
            ->setParameter('user', $user)
            ->setParameter('relation', $relation)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findByActive($user)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.asker = :user')
            ->orWhere('a.asked = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
            ;
    }
}
