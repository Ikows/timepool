<?php

namespace App\Repository;

use App\Entity\SharedCalendar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SharedCalendar|null find($id, $lockMode = null, $lockVersion = null)
 * @method SharedCalendar|null findOneBy(array $criteria, array $orderBy = null)
 * @method SharedCalendar[]    findAll()
 * @method SharedCalendar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SharedCalendarRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SharedCalendar::class);
    }

    // /**
    //  * @return SharedCalendar[] Returns an array of SharedCalendar objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SharedCalendar
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
