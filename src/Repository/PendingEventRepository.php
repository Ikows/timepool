<?php

namespace App\Repository;

use App\Entity\PendingEvent;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PendingEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method PendingEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method PendingEvent[]    findAll()
 * @method PendingEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PendingEventRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PendingEvent::class);
    }

    public function findByEventCreator(User $user)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.creator = :user')
            ->setParameter('user', serialize($user))
            ->orderBy('p.startTime', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByEventAttender(User $user)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.attenders = :user')
            ->setParameter('user', serialize($user))
            ->orderBy('p.startTime', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return PendingEvent[] Returns an array of PendingEvent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PendingEvent
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
