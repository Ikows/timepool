<?php
/**
 * Created by PhpStorm.
 * User: ikows
 * Date: 14/01/19
 * Time: 10:46
 */

namespace App\Service;

use App\Entity\PendingEvent;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class EventCreator
{
    /**
     * @param $day
     * @param $hour
     * @param $min
     * @return \Google_Service_Calendar_EventDateTime
     * @throws \Exception
     */
    private function dateFactor($day, $hour, $min)
    {
        $date = new \DateTime($day . ' ' . $hour . ':' . $min. ':00');
        $googleDate = new \Google_Service_Calendar_EventDateTime();
        $googleDate->setDateTime($date->format(DATE_ISO8601));
        return $googleDate;
    }

    /**
     * @param \DateTimeInterface $datetime
     * @return \Google_Service_Calendar_EventDateTime
     */
    private function dateTimeFactor(\DateTimeInterface $datetime)
    {
        $googleDate = new \Google_Service_Calendar_EventDateTime();
        $googleDate->setDateTime($datetime->format(DATE_ISO8601));
        return $googleDate;
    }

    /**
     * @param User $user
     * @param \Google_Client $client
     * @param array $data
     * @return bool
     */
    public function create(User $user, \Google_Client $client, array $data)
    {
        $event = new \Google_Service_Calendar_Event();

        $event->setStart($this->dateFactor($data['startDay'], $data['startHour'], $data['startMin']));
        $event->setEnd($this->dateFactor($data['endDay'], $data['endHour'], $data['endMin']));
        $event->setSummary($data['summary']);
        $event->setLocation($data['location']);
        $event->setDescription($data['description']);
        $calAPI = new \Google_Service_Calendar($client);
        $calAPI->events->insert($user->getAddEventCalendar(), $event);

        return true;
    }

    /**
     * @param array $attenders
     * @param ObjectManager $manager
     * @param PendingEvent $pendingEvent
     * @return bool
     */
    public function createMultiple(array $attenders, ObjectManager $manager, PendingEvent $pendingEvent)
    {
        foreach ($attenders as $attender) {
            //Fetching Google Client with users auth
            $client = Clienting::goClient($attender, $manager);

            $event = new \Google_Service_Calendar_Event();
            $event->setStart($this->dateTimeFactor($pendingEvent->getStartTime()));
            $event->setEnd($this->dateTimeFactor($pendingEvent->getEndTime()));
            $event->setSummary($pendingEvent->getSummary());
            $event->setLocation($pendingEvent->getLocation());
            $event->setDescription($pendingEvent->getDescription());
            $calAPI = new \Google_Service_Calendar($client);
            $calAPI->events->insert($attender->getAddEventCalendar(), $event);
        }
        return true;
    }
}
