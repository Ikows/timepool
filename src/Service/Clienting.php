<?php
/**
 * Created by PhpStorm.
 * User: ikows
 * Date: 05/01/19
 * Time: 14:12
 */

namespace App\Service;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use League\OAuth2\Client\Token\AccessToken;

class Clienting
{
    /**
     * Classe permettant d'authentifier l'utilisateur dans le Google Client afin d'acceder à l'API Calendar.
     * Permet également de rafraichir le token si il est expiré.
     *
     * @param User $user
     * @param ObjectManager $em
     * @return \Google_Client
     */
    public static function goClient(User $user, ObjectManager $em)
    {
        $client =new \Google_Client();
        if (is_array($user->getGoogleToken())) {
            $token = new AccessToken($user->getGoogleToken());
            $client->setAccessToken($token->getToken());
            $user->setGoogleToken($token);
            $em->persist($user);
            $em->flush();
        } else {
            $client->setAccessToken($user->getGoogleToken()->getToken());
        }
        $client->setClientId('332008922711-2hcqt7q6bcf0iou4ki0phb1cn3n8ba99.apps.googleusercontent.com');
        $client->setClientSecret('noYp-OvGlcJNh_gaS0JMFisD');
        //Si le token est expiré, on en cherche un autre puis on le stocke en BDD à la place de l'ancien.
        if ($user->getGoogleToken()->hasExpired()) {
            $client->fetchAccessTokenWithRefreshToken($user->getGoogleToken()->getRefreshToken());
            /*$fetchedToken = $client->getAccessToken();
            $newToken = new AccessToken($fetchedToken);
            $user->setGoogleToken($newToken);
            $em->persist($user);
            $em->flush();*/
        }
        return $client;
    }
}
