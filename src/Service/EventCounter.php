<?php
/**
 * Created by PhpStorm.
 * User: ikows
 * Date: 15/01/19
 * Time: 22:49
 */

namespace App\Service;

class EventCounter
{
    public static function countEvents($createdEvents, $attendingEvents)
    {
        $data = ['blue' => 0, 'green' => 0, 'cBlue' => 0, 'cGreen' => 0];

        foreach ($createdEvents as $createdEvent) {
            if ($createdEvent->getState() == 0) {
                $data['cRed']++;
            } elseif ($createdEvent->getState() == 1) {
                $data['cBlue']++;
            } else {
                $data['cGreen']++;
            }
        }

        foreach ($attendingEvents as $attendingEvent) {
            if ($attendingEvent->getState() == 0) {
                $data['red']++;
            } elseif ($attendingEvent->getState() == 1) {
                $data['blue']++;
            } else {
                $data['green']++;
            }
        }
        return $data;
    }
}
