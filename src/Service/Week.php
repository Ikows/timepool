<?php
/**
 * Created by PhpStorm.
 * User: ikows
 * Date: 07/12/18
 * Time: 10:43
 */

namespace App\Service;

class Week
{
    /*public $days = ['Monday', 'Thursday', 'Wednesday', 'Tuesday', 'Friday', 'Saturday', 'Sunday'];*/
    private $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
        'October', 'November', 'December'];
    public $month;
    public $year;
    public $week;
    
    /**
     * Week constructor.
     * @param int|null $day
     * @param int|null $month
     * @param int|null $year
     * @throws \Exception
     */
    public function __construct(?int $day = null, ?int $month = null, ?int $year = null)
    {
        if ($day === null) {
            $day = intval(date('d'));
        }

        if ($month === null || $month < 1 || $month > 12) {
            $month = intval(date('m'));
        }

        if ($year === null) {
            $year = intval(date('Y'));
        }

        if ($year < 1970) {
            throw new \Exception('Omg, l\'année n\'est pas valide');
        }
        $this->month = $month;
        $this->year = $year;
        $this->day = $day;
    }
    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->months[$this->month - 1] . ' ' . $this->year;
    }

    /**
     * @return Week
     * @throws \Exception
     */
    public function nextWeek()
    {

        $nowDate = new \DateTime($this->year . '-' . $this->month . '-' . $this->day);
        $nowDate->modify('+7 days');
        
        $day= $nowDate->format('d');
        $month = $nowDate->format('m');
        $year = $nowDate->format('Y');
 
        return new Week($day, $month, $year);
    }

    /**
     * @return Week
     * @throws \Exception
     */
    public function prevWeek()
    {
        
        $nowDate = new \DateTime($this->year . '-' . $this->month . '-' . $this->day);
        $nowDate->modify('-7 days');
        
        $day= $nowDate->format('d');
        $month = $nowDate->format('m');
        $year = $nowDate->format('Y');
        
        return new Week($day, $month, $year);
    }

    public function showDate()
    {
        $nowDate = new \DateTime($this->year . '-' . $this->month . '-' . $this->day);
        if ($nowDate->format('D') != 'Mon') {
            $nowDate->modify('last monday');
        }
        return $nowDate;
    }

    public function showDateEnd()
    {
        $nowDate = new \DateTime($this->year . '-' . $this->month . '-' . $this->day);
        if ($nowDate->format('D') != 'Mon') {
            $nowDate = $nowDate->modify('last monday');
        }
        $nowDate->modify('+6 days');
        return $nowDate;
    }
}
