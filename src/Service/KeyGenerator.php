<?php
/**
 * Created by PhpStorm.
 * User: ikows
 * Date: 04/01/19
 * Time: 16:03
 */

namespace App\Service;

class KeyGenerator
{
    public static function generate()
    {
        $alphabet = '0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPMLKJHGFDSQWXCVBN';
        return substr(str_shuffle(str_repeat($alphabet, 50)), 0, 50);
    }
}
