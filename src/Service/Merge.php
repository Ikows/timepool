<?php
/**
 * Created by PhpStorm.
 * User: ikows
 * Date: 06/01/19
 * Time: 15:03
 */

namespace App\Service;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class Merge
{
    /**
     * @param \Google_Client $client
     * @param User $user
     * @param Week|null $week
     * @param array|null $options
     * @return array
     * @throws \Exception
     */
    public static function merge(\Google_Client $client, User $user, Week $week = null, array $options = null)
    {
        if (!$week) {
            $week = new Week();
        }

        if (isset($options['starting'])) {
            $timeMin = $week->showDate()->modify('+' . $options['starting'] . 'week');
        } else {
            //Fetching date actually displayed
            $timeMin = $week->showDate();
        }

        $googleOptions = [
            'singleEvents' => true,
            'orderBy' => 'StartTime',
            'timeMin' => $timeMin->format(DATE_RFC3339)
        ];


        if (isset($options['timeMax'])) {
            $googleOptions += ['timeMax' => $timeMin->modify('+'.$options['timeMax'].' week')->format(DATE_RFC3339)];
        }

        $calendarApi = new \Google_Service_Calendar($client);

        //Creating an array which contains the startTime and the endTime for each event
        $events = [];
        foreach ($user->getSelectedCalendar() as $value) {
            $items = $calendarApi->events->listEvents($value, $googleOptions)->getItems();
            foreach ($items as $key => $item) {
                $startTime = new \DateTime($item->getStart()->getDateTime());
                $endTime = new \DateTime($item->getEnd()->getDateTime());

                //Rounding the start time of an event
                //to ensure it's 00, 15, 30 or 45 min to be correctly showed on calendar
                $startMin = $startTime->format('i');
                if ($startMin != 00 and $startMin != 15 and $startMin != 30 and $startMin != 45) {
                    if ($startMin < 8) {
                        ($startTime->setTime($startTime->format('h'), 00));
                    } elseif ($startMin >= 8 and $startMin < 23) {
                        $startTime->setTime($startTime->format('h'), 15);
                    } elseif ($startMin >= 23 and $startMin < 38) {
                        $startTime->setTime($startTime->format('h'), 30);
                    } elseif ($startMin >= 38 and $startMin < 53) {
                        $startTime->setTime($startTime->format('h'), 45);
                    } else {
                        $startTime->setTime($startTime->format('h'), 00);
                        $startTime->modify('+1 hour');
                    }
                }
                $events[] = [
                    'startTime' => $startTime,
                    'endTime' => $endTime
                ];
            }
        }

        //Creating an array which contains, as key, the dateTime corresponding to one of the calendar cell to fill
        $event = [];
        foreach ($events as $evento) {
            $event += [$evento['startTime']->format('Y-m-d H:i:s') => true];
            $diff = ($evento['endTime']->getTimeStamp() - $evento['startTime']->getTimeStamp()) / 900;
            for ($i = 1; $i < $diff; $i++) {
                $event += [$evento['startTime']->modify('+15 minutes')->format('Y-m-d H:i:s') => true];
            }
        }
        return $event;
    }

    public static function mergeMultiple(array $users, ObjectManager $em)
    {
        $events = [];
        foreach ($users as $user) {
            $client = Clienting::goClient($user, $em);
            $calendarApi = new \Google_Service_Calendar($client);
            foreach ($user->getSelectedCalendar() as $value) {
                $items = $calendarApi->events->listEvents($value, [
                    'singleEvents' => true,
                    'orderBy' => 'StartTime'
                ])->getItems();
                foreach ($items as $key => $item) {
                    $events[] = [
                        'startTime' => new \DateTime($item->getStart()->getDateTime()),
                        'endTime' => new \DateTime($item->getEnd()->getDateTime())
                    ];
                }
            }
        }
        $event = [];
        foreach ($events as $evento) {
            $event += [$evento['startTime']->format('Y-m-d H:i:s') => true];
            $diff = ($evento['endTime']->getTimeStamp() - $evento['startTime']->getTimeStamp()) / 900;
            for ($i = 1; $i < $diff; $i++) {
                $event += [$evento['startTime']->modify('+15 minutes')->format('Y-m-d H:i:s') => true];
            }
        }
        return $event;
    }
}
