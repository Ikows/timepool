<?php


namespace App\Notification;

use Twig\Environment;

class ContactNotification
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $renderer;

    public function __construct(\Swift_Mailer $mailer, Environment $renderer)
    {

        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    public function notify($data)
    {

        $message = (new \Swift_Message($data["subject"]))
            ->setFrom('legrand.jessica45@gmail.com')
            ->setTo($data["email"])
            ->setBody($this->renderer->render('footer_contents/email.html.twig'), 'text/html');
        $this->mailer->send($message);
    }
}
