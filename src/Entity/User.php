<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;


    /**
     * @Assert\Length(min="2")
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $familyName;

    /**
     * @Assert\Length(min="2")
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $givenName;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $selectedCalendar = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $unselectedCalendar = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picture;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     */
    private $relations;

    /**
     * @ORM\Column(type="object")
     */
    private $googleToken;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fullname;

    /**
     * @ORM\Column(type="boolean")
     */
    private $display;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $addEventCalendar;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(max=500, maxMessage="Unfortunately, your presentation cannot exceed 500 characters")
     */
    private $biography;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $webLink;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PendingEvent", inversedBy="attenders")
     */
    private $attending;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReceivedMessage", mappedBy="user", cascade={"remove"})
     */
    private $receivedMessages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SentMessage", mappedBy="user", cascade={"remove"})
     */
    private $sentMessages;

    public function __construct()
    {
        $this->relations = new ArrayCollection();
        $this->attending = new ArrayCollection();
        $this->receivedMessages = new ArrayCollection();
        $this->sentMessages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFamilyName(): ?string
    {
        return $this->familyName;
    }

    public function setFamilyName(string $familyName): self
    {
        $this->familyName = $familyName;

        return $this;
    }

    public function getGivenName(): ?string
    {
        return $this->givenName;
    }

    public function setGivenName(string $givenName): self
    {
        $this->givenName = $givenName;

        return $this;
    }

    public function getSelectedCalendar(): ?array
    {
        return $this->selectedCalendar;
    }

    public function setSelectedCalendar(?array $selectedCalendar): self
    {
        $this->selectedCalendar = $selectedCalendar;

        return $this;
    }

    public function getUnselectedCalendar(): ?array
    {
        return $this->unselectedCalendar;
    }

    public function setUnselectedCalendar(?array $unselectedCalendar): self
    {
        $this->unselectedCalendar = $unselectedCalendar;

        return $this;
    }

    public function getPicture()
    {
        return $this->picture;
    }

    public function setPicture($picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getRelations(): Collection
    {
        return $this->relations;
    }

    public function addRelation(self $relation): self
    {
        if (!$this->relations->contains($relation)) {
            $this->relations[] = $relation;
        }

        return $this;
    }

    public function removeRelation(self $relation): self
    {
        if ($this->relations->contains($relation)) {
            $this->relations->removeElement($relation);
        }

        return $this;
    }

    public function getGoogleToken()
    {
        return $this->googleToken;
    }

    public function setGoogleToken($googleToken): self
    {
        $this->googleToken = $googleToken;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getDisplay(): ?bool
    {
        return $this->display;
    }

    public function setDisplay(bool $display): self
    {
        $this->display = $display;

        return $this;
    }

    public function getAddEventCalendar(): ?string
    {
        return $this->addEventCalendar;
    }

    public function setAddEventCalendar(string $addEventCalendar): self
    {
        $this->addEventCalendar = $addEventCalendar;

        return $this;
    }

    public function getBiography(): ?string
    {
        return $this->biography;
    }

    public function setBiography(?string $biography): self
    {
        $this->biography = $biography;

        return $this;
    }

    public function getWebLink(): ?string
    {
        return $this->webLink;
    }

    public function setWebLink(?string $webLink): self
    {
        $this->webLink = $webLink;

        return $this;
    }

    /**
     * @return Collection|PendingEvent[]
     */
    public function getAttending(): Collection
    {
        return $this->attending;
    }

    public function addAttending(PendingEvent $attending): self
    {
        if (!$this->attending->contains($attending)) {
            $this->attending[] = $attending;
        }

        return $this;
    }

    public function removeAttending(PendingEvent $attending): self
    {
        if ($this->attending->contains($attending)) {
            $this->attending->removeElement($attending);
        }

        return $this;
    }

    /**
     * @return Collection|ReceivedMessage[]
     */
    public function getReceivedMessages(): Collection
    {
        return $this->receivedMessages;
    }

    public function addReceivedMessage(ReceivedMessage $receivedMessage): self
    {
        if (!$this->receivedMessages->contains($receivedMessage)) {
            $this->receivedMessages[] = $receivedMessage;
            $receivedMessage->setUser($this);
        }

        return $this;
    }

    public function removeReceivedMessage(ReceivedMessage $receivedMessage): self
    {
        if ($this->receivedMessages->contains($receivedMessage)) {
            $this->receivedMessages->removeElement($receivedMessage);
            // set the owning side to null (unless already changed)
            if ($receivedMessage->getUser() === $this) {
                $receivedMessage->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SentMessage[]
     */
    public function getSentMessages(): Collection
    {
        return $this->sentMessages;
    }

    public function addSentMessage(SentMessage $sentMessage): self
    {
        if (!$this->sentMessages->contains($sentMessage)) {
            $this->sentMessages[] = $sentMessage;
            $sentMessage->setUser($this);
        }

        return $this;
    }

    public function removeSentMessage(SentMessage $sentMessage): self
    {
        if ($this->sentMessages->contains($sentMessage)) {
            $this->sentMessages->removeElement($sentMessage);
            // set the owning side to null (unless already changed)
            if ($sentMessage->getUser() === $this) {
                $sentMessage->setUser(null);
            }
        }

        return $this;
    }
}
