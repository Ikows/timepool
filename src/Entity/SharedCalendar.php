<?php

namespace App\Entity;

use App\Service\KeyGenerator;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SharedCalendarRepository")
 */
class SharedCalendar
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="array")
     */
    private $event = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sand;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $weekRange = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startFrom;

    public function __construct($event)
    {
        $this->event = $event;
        $this->sand = KeyGenerator::generate();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvent(): ?array
    {
        return $this->event;
    }

    public function setEvent(array $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getSand(): ?string
    {
        return $this->sand;
    }

    public function setSand(string $sand): self
    {
        $this->sand = $sand;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getWeekRange(): ?int
    {
        return $this->weekRange;
    }

    public function setWeekRange(int $weekRange): self
    {
        $this->weekRange = $weekRange;

        return $this;
    }

    public function getStartFrom(): ?\DateTimeInterface
    {
        return $this->startFrom;
    }

    public function setStartFrom(\DateTimeInterface $startFrom): self
    {
        $this->startFrom = $startFrom;

        return $this;
    }
}
