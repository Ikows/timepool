<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AddingRepository")
 */
class Adding
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cle;

    /**
     * @ORM\Column(type="integer")
     */
    private $asker;

    /**
     * @ORM\Column(type="integer")
     */
    private $asked;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCle(): ?string
    {
        return $this->cle;
    }

    public function setCle(string $cle): self
    {
        $this->cle = $cle;

        return $this;
    }

    public function getAsker(): ?int
    {
        return $this->asker;
    }

    public function setAsker(int $asker): self
    {
        $this->asker = $asker;

        return $this;
    }

    public function getAsked(): ?int
    {
        return $this->asked;
    }

    public function setAsked(int $asked): self
    {
        $this->asked = $asked;

        return $this;
    }
}
