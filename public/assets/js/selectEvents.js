$(document).ready(function() {
    // you may need to change this code if you are not using Bootstrap Datepicker
    $('.js-datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
});

$('.js-datepicker').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

$('.heureDispo').hover(function () {
    $(this).find('p').toggleClass('d-none');
    $(this).find('i').toggleClass('mr-3 d-none');
});

$('.heureDispo').click(function () {
    let sel = $(this).find('.start').text();
    if (sel < 10) {
        sel = '0'+sel;
    }
    let selE = $(this).find('.end').text();
    if (selE < 10) {
        selE = '0'+selE;
    }
    $('#add_event_startHour option[value="'+sel+'"]').prop('selected', true);
    $('#add_event_endHour option[value="'+selE+'"]').prop('selected', true);
    $('#add_event_startDay').val($(this).find('div').text());
    $('#add_event_endDay').val($(this).find('div').text());
    $('#form_startMin option[value="0"]').prop('selected', true);
    $('#form_endMin option[value="0"]').prop('selected', true);
});

$('.nextPrev').click(function () {
    $(this).text('');
    $(this).append('<i class="fas fa-spinner fa-pulse my-0 mx-0"></i>')
});