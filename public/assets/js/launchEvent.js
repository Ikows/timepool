$('#launchButton').click(function () {
    $(this).removeClass('bounceInDown animated');
    $(this).text('');
    $(this).append($('<p>Powered by <h3>TimePool</h3></p><i class="fas fa-spinner fa-pulse my-0 mx-0"></i>'));
    $(this).addClass('animated infinite pulse');
});

$('#unlock').click(function () {
    $(this).toggleClass('fa-lock fa-lock-open');
    $('#launchButton').toggleClass('bounceOutUp');
    setTimeout(function () {
        $('#launchButton').toggleClass('d-none bounceInDown');
    }, 700)
});

$('.accept').click(function(e) {
    e.preventDefault();

    let url = $(this).attr('href');

    xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            $('#attender'+$('#appUser').val()+'').toggleClass('badge-primary badge-success');
            $('#waiting').children().remove();
            $('#waiting').append('        <div class="row justify-content-around my-4">\n'+
                '            <div class="col-md-6">\n'+
                '                <button class="btn btn-lg btn-success w-100">You\'ve accepted the invitation</button>\n'+
                '            </div>\n'+
                '        </div>')
        }
    };

    xhttp.open('GET', url, true);
    xhttp.send();
})

$('.decline').click(function(e) {
    e.preventDefault();

    let url = $(this).attr('href');

    xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            $('#attender'+$('#appUser').val()+'').toggleClass('badge-primary badge-danger');
            $('#waiting').children().remove();
            $('#waiting').append('        <div class="row justify-content-around my-4">\n'+
                '            <div class="col-md-6">\n'+
                '                <button class="btn btn-lg btn-danger w-100">You\'ve declined the invitation</button>\n'+
                '            </div>\n'+
                '        </div>')
        }
    };

    xhttp.open('GET', url, true);
    xhttp.send();
})