$('#notAddedRow').on('click', '.plus', function () {
    let ide = $(this).attr('id');
    $('#'+ide).parent().prev().children().toggleClass('fullname added');
    $(this).parent().parent().toggleClass('animated bounceOutRight');
    setTimeout(function () {
        let slider = $('#'+ide);
        slider.parent().parent().toggleClass('bounceInLeft bounceOutRight');
        $('#addedRow').append($('#div'+ide));
        $('#addedRow').append('<input type="hidden" id="input'+ide+'" name="rel[input'+ide+']" value="'+ide+'">');
        slider.removeClass('fa-plus-square plus');
        slider.addClass('moins fa-minus-square');
        slider.css('color', 'red');
    },500);
    setTimeout(function () {
        $('#'+ide).parent().parent().toggleClass('animated bounceInLeft');
    },1500);
});

$('#addedRow').on('click', '.moins', function () {
    let ide = $(this).attr('id');
    $('#'+ide).parent().prev().children().toggleClass('fullname added');
    $('#input'+ide).remove();
    $('#'+ide).parent().parent().toggleClass('animated bounceOutLeft');
    setTimeout(function () {
        let slider = $('#'+ide);
        slider.parent().parent().toggleClass('bounceInRight bounceOutLeft');
        $('#notAddedRow').append($('#div'+ide));
        slider.removeClass('moins fa-minus-square');
        slider.addClass('fa-plus-square plus');
        slider.css('color', 'green');
    },500);
    setTimeout(function () {
        $('#'+ide).parent().parent().toggleClass('animated bounceInRight');
    },1500);
});

$('#form_submit').click(function () {
    $(this).text('');
    $(this).append('<i class="fas fa-spinner fa-pulse my-0 mx-0"></i>');
    $('.getOut').addClass('animated bounceOutUp');
    $('.goOut').addClass('animated hinge');
    setTimeout(function () {
        $('.loading').toggleClass('d-none');
    }, 800)
});

